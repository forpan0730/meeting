﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Iconsole.Model.Pages.ProductionManagement.UtilizationRate.UtilizationRateModel;

namespace Iconsole.Pages.ProductionManagement.PiblicFunction
{
	public class ReadedProgramResumeDataBase
	{
		/// <summary>	
		/// 找起始到結束期間內"每天的時間總和"的資料
		/// </summary>
		/// <param name="startTime">起始時間</param>
		/// <param name="endTime">結束時間</param>
		/// <returns></returns>
		public List<ResultClass> GetWorkData(DateTime startTime, DateTime endTime)
		{
			return CountDatasAWeek(CorrectionAllDataByDate(startTime, endTime), startTime, endTime);
		}

		/// <summary>
		/// 一星期"每天的時間總和"資料
		/// </summary>
		/// <returns></returns>
		public List<ResultClass> GetWorkData()
		{
			var now = DateTime.Today.Date;
			var weekAgo = now.AddDays(-6);
			var todayover = now.AddDays(1);
			
			return CountDatasAWeek(CorrectionAllDataByDate(weekAgo, todayover).Where(w => w.IsAlarm == false), weekAgo, todayover).ToList();
		}


		/// <summary>
		/// 一星期"每天的時間總和"資料
		/// </summary>
		/// <returns></returns>
		public List<ResultClass> GetAlarmData()
		{
			var now = DateTime.Today.Date;
			var weekAgo = now.AddDays(-6);
			var todayover = now.AddDays(1);

			return CountDatasAWeek(CorrectionAllDataByDate(weekAgo, todayover).Where(w => w.IsAlarm == true), weekAgo, todayover).ToList();
		}

		/// <summary>	
		/// 找起始到結束期間內"每天的每筆工作資料"的資料
		/// </summary>
		/// <param name="startTime">起始時間</param>
		/// <param name="endTime">結束時間</param>
		/// <returns></returns>
		public IEnumerable<ResultClass> GetAllWorkData(DateTime startTime, DateTime endTime)
		{	
			var searchend = endTime.AddDays(1);

			var result = CorrectionAllDataByDate(startTime, searchend)
							.Concat(Enumerable.Range(0, ((searchend - startTime).Days))
							.Select(x => new ResultClass {Id= x, Startdate = searchend.AddDays(-x), Enddate = searchend.AddDays(-x), TotalSeconds = 0, IsAlarm = false }))
							.Where(w => w.Startdate >= startTime && w.Startdate <= searchend);
			return result;
		}


		public List<double> TimeSeconedsAWeek()
		{
			List<double> totaldaySecond = new List<double>();

			foreach (var item in GetWorkData())
			{
				totaldaySecond.Add(item.TotalSeconds);
			}

			return totaldaySecond;
		}

		public List<double> AlarmTimeSeconedsAWeek()
		{
			List<double> totalAlarmSecond = new List<double>();

			foreach (var item in GetAlarmData())
			{
				totalAlarmSecond.Add(item.TotalSeconds);
			}

			return totalAlarmSecond;
		}

		/// <summary>
		/// 一周常用程式
		/// </summary>
		/// <param name="count"></param>
		/// <returns></returns>
		public List<Program> ReadCommonProgramAWeek(int count)
		{
			var today = DateTime.Today.Date;
			var targetdate = today.AddDays(count * -1);
			var targetnextdate = targetdate.AddDays(1);


			var result = CorrectionAllDataByDate(targetdate, targetnextdate).Where(w => w.Startdate >= targetdate && w.Startdate < targetnextdate)
							.Select(s => new { s.ProgramName })
							.GroupBy(g => g.ProgramName)
							.Select(x => new Program
							{
								ProgramName = x.Key,
								ProgramUsedCount = x.Count()
							})
							.OrderByDescending(x => x.ProgramUsedCount)
							.ToList()
							.Concat(Enumerable.Range(0, 2).Select(x => new Program { ProgramName = "", ProgramUsedCount = 0.0 }))
							.ToList();

			return result;
		}

		//資料整合 -單位:天	
		private List<ResultClass> CountDatasAWeek(IEnumerable<ResultClass> workdatalist, DateTime startTime, DateTime endTime)
		{
			//var result = new List<ResultClass>();

			var result = workdatalist.Concat(Enumerable.Range(0, (endTime - startTime).Days)
						.Select(x => new ResultClass { Startdate = DateTime.Today.AddDays(-x), Enddate = DateTime.Today.AddDays(-x), TotalSeconds = 0 }))
						.GroupBy(x => x.Startdate.Date)
						.Select(x => new ResultClass { Startdate = x.Key, TotalSeconds = (x.Sum(y => y.TotalSeconds)) })
						.OrderBy(x => x.Startdate)
						.Where(w => w.Startdate >= startTime && w.Startdate < endTime)
		                .ToList();
				
			
			

			return result;
		}

		public List<ResultClass> ReadDataBaseWithDate(DateTime startTime, DateTime endTime)
		{
			List<ResultClass> ProgramDateAndTime = new List<ResultClass>();

			using (var DB = new ResumeDBEntities())
			{
				var endday = endTime.AddDays(1);


				ProgramDateAndTime = DB.Resume
							.Where(w => startTime <= w.End)
							.Select(s => new ResultClass
							{
								Id = s.No,
								Startdate = s.Start.Value,
								Enddate = s.End.Value,
								WorkTime = s.ProgramTime,
								TotalSeconds = s.TotalSecond.Value,
								ProgramName = s.ProgramName,
								EndStatus = s.Status,
								IsAlarm = s.IsAlarm,
							}).ToList();
			}

			return ProgramDateAndTime;
		}

		private List<ResultClass> CorrectionAllDataByDate(DateTime startTime, DateTime endTime)
		{
			List<ResultClass> resultList = new List<ResultClass>();

			var dateProgramResume = ReadDataBaseWithDate(startTime, endTime);

			foreach (var item in dateProgramResume)
			{
				//資料的開始與結束
				var workfrom = item.Startdate;
				var workto = item.Enddate;
				//初始化
				DateTime start = DateTime.Today;
				DateTime end = DateTime.Today;

				int days = (workto.Date - workfrom.Date).Days;

				for (int i = 0; i <= days; i++)
				{
					if ((workto - workfrom).TotalSeconds > 86400)
					{
						start = workto.AddSeconds(-1).Date;
						end = workto;
					}
					else if ((workto - workfrom).TotalSeconds < 86400 && workto.AddSeconds(-1).Date != workfrom.Date)
					{
						start = workto.AddSeconds(-1).Date;
						end = workto;
					}
					else
					{
						start = workfrom;
						end = workto;
					}

					var worktime = end - start;

					if (worktime.Days== 1)
					{
						worktime = new TimeSpan(23, 59, 59);
					}
										
					resultList.Add(new ResultClass()
					{
						Id = i,
						Startdate = start,
						Enddate = end,
						WorkTime = worktime.ToString(@"hh\:mm\:ss"),
						TotalSeconds = (int)(end - start).TotalSeconds,
						ProgramName = item.ProgramName,
						EndStatus = item.EndStatus,
						IsAlarm = item.IsAlarm
					});

					workto = start;
				}
			}

			return resultList;
		}
	}
}

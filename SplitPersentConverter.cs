﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Iconsole.Pages.ProductionManagement.UtilizationRate.Converters
{
	public class SplitPersentConverter : IValueConverter
	{
		double result = 0;
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null)
			{
				var persent = value.ToString();

				double.TryParse(persent.Substring(0, persent.Length - 1), out result);
			}
			return result;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
